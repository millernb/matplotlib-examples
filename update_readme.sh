#!/bin/bash

rm -r examples_files
jupyter nbconvert --execute --to markdown examples.ipynb
mv examples.md README.md
